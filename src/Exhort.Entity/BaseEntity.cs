﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity
{
    /// <summary>
    /// 实体基类
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// 主键字段
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 插入时间
        /// </summary>
        public DateTime InsertTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 初始化编号
        /// </summary>
        public void InitializeID(bool isNewID = false)
        {
            if (string.IsNullOrEmpty(ID) || isNewID)
            {
                // 设置简短版 Guid 编号
                ID = Guid.NewGuid().ToString().Replace("-", string.Empty);
            }
        }
    }
}