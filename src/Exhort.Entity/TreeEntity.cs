﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity
{
    /// <summary>
    /// 树形基类
    /// </summary>
    public abstract class TreeEntity : OrderEntity
    {
        /// <summary>
        /// 父级字段
        /// </summary>
        public string Pid { get; set; }

        /// <summary>
        /// 路径字段
        /// </summary>
        public string Path { get; set; }
    }
}