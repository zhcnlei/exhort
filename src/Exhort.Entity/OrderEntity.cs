﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity
{
    /// <summary>
    /// 排序基类
    /// </summary>
    public abstract class OrderEntity : BaseEntity
    {
        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sequence { get; set; }
    }
}