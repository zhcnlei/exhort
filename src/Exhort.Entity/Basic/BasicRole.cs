﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicRole : OrderEntity
    {
        public string Title { get; set; }
        public bool IsEnabled { get; set; }
        public string Remark { get; set; }
    }
}