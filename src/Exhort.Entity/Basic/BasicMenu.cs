﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicMenu : TreeEntity
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
        public string Module { get; set; }
        public string Value { get; set; }
        public string Src { get; set; }
        public bool IsEnabled { get; set; }
    }
}