﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicUser : BaseEntity
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Name { get; set; }
        public string Head { get; set; }
        public string Mail { get; set; }
        public string Tag { get; set; }
        public bool IsSuper { get; set; }
        public bool IsEnabled { get; set; }
        public string Remark { get; set; }
    }
}