﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicRoleMenuFunction : BaseEntity
    {
        public string RoleID { get; set; }
        public string MenuFunctionID { get; set; }

        public BasicRole BasicRole { get; set; }
        public BasicMenuFunction BasicMenuFunction { get; set; }
    }
}