﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicSetting : BaseEntity
    {
        public string Tag { get; set; }
        public string Content { get; set; }
        public string Other { get; set; }
    }
}