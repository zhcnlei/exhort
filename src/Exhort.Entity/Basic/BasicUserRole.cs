﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicUserRole : BaseEntity
    {
        public string UserID { get; set; }
        public string RoleID { get; set; }

        public BasicUser BasicUser { get; set; }
        public BasicRole BasicRole { get; set; }
    }
}