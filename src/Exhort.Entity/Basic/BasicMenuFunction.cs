﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicMenuFunction : OrderEntity
    {
        public string MenuID { get; set; }
        public string FunctionID { get; set; }

        public BasicMenu BasicMenu { get; set; }
        public BasicFunction BasicFunction { get; set; }
    }
}