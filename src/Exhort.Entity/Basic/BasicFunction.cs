﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Entity.Basic
{
    public class BasicFunction : OrderEntity
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Attrs { get; set; }
        public string Remark { get; set; }
    }
}