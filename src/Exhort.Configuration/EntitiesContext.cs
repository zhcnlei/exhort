﻿using Exhort.Dapper;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;

namespace Exhort.Configuration
{
    public partial class EntitiesContext : DbContext
    {
        /// <summary>
        /// 数据连接
        /// </summary>
        public IDbConnection Connection { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="providerName"></param>
        public EntitiesContext(string connString, string providerName)
        {
            Connection = DbFactory.GetDbConnection(connString, providerName);
        }

        /// <summary>
        /// 注册数据库
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Connection as DbConnection);
        }

        /// <summary>
        /// 实体注册
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 初始化实体模型生成器
            EntityConfigurationFactory.ConfigurationsInit(modelBuilder);
        }
    }
}