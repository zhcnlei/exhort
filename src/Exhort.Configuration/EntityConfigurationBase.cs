﻿using Exhort.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration
{
    /// <summary>
    /// 实体配置器
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    internal abstract class EntityConfigurationBase<T> : IEntityTypeConfiguration<T>, IEntityConfiguration where T : BaseEntity
    {
        /// <summary>
        /// 实体映射
        /// </summary>
        public void Configure(EntityTypeBuilder<T> builder)
        {
            builder.ToTable(TableName);
            PrimaryInitialization(builder);
            MapperInitialization(builder);
        }

        /// <summary>
        /// 默认表名
        /// </summary>
        internal abstract string TableName { get; }

        /// <summary>
        /// 设置主键
        /// </summary>
        internal virtual void PrimaryInitialization(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(m => m.ID);
        }

        /// <summary>
        /// 设置映射
        /// </summary>
        internal virtual void MapperInitialization(EntityTypeBuilder<T> builder)
        {

        }

        /// <summary>
        /// 将实体配置对象注册到实体生成器配置集合
        /// </summary>
        /// <param name="modelBuilder">实体生成器配置集合</param>
        public void AddTo(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(this);
        }
    }
}