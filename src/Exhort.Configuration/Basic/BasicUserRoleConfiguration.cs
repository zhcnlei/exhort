using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicUserRoleConfiguration : EntityConfigurationBase<BasicUserRole>
    {
        internal override string TableName => "Basic_UserRole";

        internal override void MapperInitialization(EntityTypeBuilder<BasicUserRole> builder)
        {
            builder.HasOne(m => m.BasicUser).WithMany().HasForeignKey(m => m.UserID);
            builder.HasOne(m => m.BasicRole).WithMany().HasForeignKey(m => m.RoleID);
        }
    }
}