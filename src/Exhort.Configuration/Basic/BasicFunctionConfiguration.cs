using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicFunctionConfiguration : EntityConfigurationBase<BasicFunction>
    {
        internal override string TableName => "Basic_Function";
    }
}