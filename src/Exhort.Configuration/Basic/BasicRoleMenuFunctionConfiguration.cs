using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicRoleMenuFunctionConfiguration : EntityConfigurationBase<BasicRoleMenuFunction>
    {
        internal override string TableName => "Basic_RoleMenuFunction";

        internal override void MapperInitialization(EntityTypeBuilder<BasicRoleMenuFunction> builder)
        {
            builder.HasOne(m => m.BasicRole).WithMany().HasForeignKey(m => m.RoleID);
            builder.HasOne(m => m.BasicMenuFunction).WithMany().HasForeignKey(m => m.MenuFunctionID);
        }
    }
}