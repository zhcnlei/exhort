using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicRoleConfiguration : EntityConfigurationBase<BasicRole>
    {
        internal override string TableName => "Basic_Role";
    }
}