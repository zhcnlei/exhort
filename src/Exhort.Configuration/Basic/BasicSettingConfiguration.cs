using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicSettingConfiguration : EntityConfigurationBase<BasicSetting>
    {
        internal override string TableName => "Basic_Setting";
    }
}