using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicMenuFunctionConfiguration : EntityConfigurationBase<BasicMenuFunction>
    {
        internal override string TableName => "Basic_MenuFunction";

        internal override void MapperInitialization(EntityTypeBuilder<BasicMenuFunction> builder)
        {
            builder.HasOne(m => m.BasicMenu).WithMany().HasForeignKey(m => m.MenuID);
            builder.HasOne(m => m.BasicFunction).WithMany().HasForeignKey(m => m.FunctionID);
        }
    }
}