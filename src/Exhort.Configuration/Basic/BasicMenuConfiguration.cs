using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicMenuConfiguration : EntityConfigurationBase<BasicMenu>
    {
        internal override string TableName => "Basic_Menu";
    }
}