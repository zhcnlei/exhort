using Exhort.Entity.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exhort.Configuration.Basic
{
    internal class BasicUserConfiguration : EntityConfigurationBase<BasicUser>
    {
        internal override string TableName => "Basic_User";
    }
}