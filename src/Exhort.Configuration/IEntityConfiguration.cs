﻿using Microsoft.EntityFrameworkCore;

namespace Exhort.Configuration
{
    /// <summary>
    /// 实体配置器
    /// </summary>
    internal interface IEntityConfiguration
    {
        /// <summary>
        /// 将实体配置对象注册到实体生成器配置集合
        /// </summary>
        /// <param name="modelBuilder">实体生成器配置集合</param>
        void AddTo(ModelBuilder modelBuilder);
    }
}