﻿using Exhort.Entity;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace Exhort.DbSet
{
    /// <summary>
    /// 基本数据
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public class BaseDbSet<T> where T : BaseEntity, new()
    {
        /// <summary>
        /// 数据上下文
        /// </summary>
        protected AffairContext dbContext = null;

        /// <summary>
        /// 设置数据对象
        /// </summary>
        /// <param name="context">数据对象</param>
        internal void Init(AffairContext context)
        {
            dbContext = context;
        }

        /// <summary>
        /// 查询计算器
        /// </summary>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Query()
        {
            return dbContext.Ef.Set<T>().AsNoTracking().OrderBy(m => m.InsertTime);
        }

        /// <summary>
        /// 查询计算器
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Query(Expression<Func<T, bool>> predicate)
        {
            return Query().Where(predicate);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="rows">行数</param>
        /// <param name="page">页码</param>
        /// <param name="entities">查询计算器</param>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Pager(int rows, int page, IQueryable<T> entities = null)
        {
            if (entities == null) { entities = Query(); }

            return entities.Skip(rows * (page - 1)).Take(rows);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="rows">行数</param>
        /// <param name="page">页码</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方式</param>
        /// <param name="entities">查询计算器</param>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Pager(int rows, int page, string sort, string order, IQueryable<T> entities = null)
        {
            if (entities == null) { entities = Query(); }

            ParameterExpression left = Expression.Parameter(typeof(T), "p");
            PropertyInfo property = typeof(T).GetProperty(sort);

            MemberExpression member = Expression.Property(left, property);
            LambdaExpression lambda = Expression.Lambda(member, left);

            order = order.Equals("asc") ? "OrderBy" : "OrderByDescending";

            Type[] types = new Type[] { typeof(T), property.PropertyType };
            UnaryExpression unary = Expression.Quote(lambda);
            Expression query = Expression.Call(typeof(Queryable), order, types, entities.Expression, unary);

            return entities.Provider.CreateQuery<T>(query).Skip(rows * (page - 1)).Take(rows);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="rows">行数</param>
        /// <param name="page">页码</param>
        /// <param name="count">数据总数</param>
        /// <param name="entities">查询计算器</param>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Pager(int rows, int page, out int count, IQueryable<T> entities = null)
        {
            if (entities == null) { entities = Query(); }

            count = entities.Count();

            return Pager(rows, page, entities);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="rows">行数</param>
        /// <param name="page">页码</param>
        /// <param name="sort">排序字段</param>
        /// <param name="order">排序方式</param>
        /// <param name="count">数据总数</param>
        /// <param name="entities">查询计算器</param>
        /// <returns>查询计算器</returns>
        public virtual IQueryable<T> Pager(int rows, int page, string sort, string order, out int count, IQueryable<T> entities = null)
        {
            if (entities == null) { entities = Query(); }

            count = entities.Count();

            return Pager(rows, page, sort, order, entities);
        }

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>实体</returns>
        public virtual T Get(string key)
        {
            return Get(m => m.ID == key);
        }

        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <returns></returns>
        public virtual T Get(Expression<Func<T, bool>> predicate)
        {
            return Query(predicate).FirstOrDefault();
        }

        #region 基础操作

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public virtual int Added(T entity)
        {
            entity.InitializeID();

            entity.InsertTime = DateTime.Now;
            entity.UpdateTime = DateTime.Now;

            dbContext.Ef.Set<T>().Attach(entity);
            dbContext.Ef.Entry<T>(entity).State = EntityState.Added;

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns>受影响行数</returns>
        public virtual int Added(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                entity.InitializeID();

                entity.InsertTime = DateTime.Now;
                entity.UpdateTime = DateTime.Now;

                dbContext.Ef.Set<T>().Attach(entity);
                dbContext.Ef.Entry<T>(entity).State = EntityState.Added;
            }

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public virtual int Modified(T entity)
        {
            entity.UpdateTime = DateTime.Now;

            dbContext.Ef.Set<T>().Attach(entity);
            dbContext.Ef.Entry<T>(entity).State = EntityState.Modified;

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public virtual int ModifiedExcept(T entity, params string[] properties)
        {
            entity.UpdateTime = DateTime.Now;

            dbContext.Ef.Set<T>().Attach(entity);
            var entry = dbContext.Ef.Entry<T>(entity);
            entry.State = EntityState.Modified;

            //排除不要更新的字段
            foreach (var item in properties)
            {
                entry.Property(item).IsModified = false;
            }

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns>受影响行数</returns>
        public virtual int Modified(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                entity.UpdateTime = DateTime.Now;

                dbContext.Ef.Set<T>().Attach(entity);
                dbContext.Ef.Entry<T>(entity).State = EntityState.Modified;
            }

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 主键删除实体
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>受影响行数</returns>
        public virtual int Deleted(string key)
        {
            return Deleted(new T() { ID = key });
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public virtual int Deleted(T entity)
        {
            dbContext.Ef.Set<T>().Attach(entity);
            dbContext.Ef.Entry<T>(entity).State = EntityState.Deleted;

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体</param>
        /// <returns>受影响行数</returns>
        public virtual int Deleted(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                dbContext.Ef.Set<T>().Attach(entity);
                dbContext.Ef.Entry<T>(entity).State = EntityState.Deleted;
            }

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 条件删除实体
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <returns>受影响行数</returns>
        public virtual int Deleted(Expression<Func<T, bool>> predicate)
        {
            return Deleted(Query(predicate));
        }

        #endregion
    }
}