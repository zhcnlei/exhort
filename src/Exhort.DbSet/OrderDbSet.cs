﻿using Exhort.Entity;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Exhort.DbSet
{
    /// <summary>
    /// 排序数据
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public class OrderDbSet<T> : BaseDbSet<T> where T : OrderEntity, new()
    {
        /// <summary>
        /// 查询计算器
        /// </summary>
        /// <returns>查询计算器</returns>
        public override IQueryable<T> Query()
        {
            return dbContext.Ef.Set<T>().AsNoTracking().OrderBy(m => m.Sequence);
        }

        /// <summary>
        /// 设置排序
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="order">方向</param>
        /// <param name="entities">排序序列</param>
        /// <returns>受影响行数</returns>
        public int Order(string id, string order, IQueryable<T> entities)
        {
            T currentEntity = Get(id);

            int temp = currentEntity.Sequence;

            #region 条件查询

            // 读取数据条件
            if (order.Equals("asc"))
            {
                entities = entities.Where(m => m.Sequence > temp).OrderBy(m => m.Sequence);
            }
            else
            {
                entities = entities.Where(m => m.Sequence < temp).OrderByDescending(m => m.Sequence);
            }

            #endregion

            #region 调换顺序

            // 读取数据
            T orderEntity = entities.FirstOrDefault();

            // 调换位置
            if (orderEntity != null)
            {
                currentEntity.Sequence = orderEntity.Sequence;
                orderEntity.Sequence = temp;
                return Modified(new List<T> { currentEntity, orderEntity });
            }

            return 0;

            #endregion
        }

        /// <summary>
        /// 获取排序字段最大值
        /// </summary>
        /// <returns>排序最大值</returns>
        public virtual int MaxSequence()
        {
            try
            {
                return Query().Max(m => m.Sequence);
            }
            catch { return 0; }
        }

        /// <summary>
        /// 添加实体重写
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public override int Added(T entity)
        {
            entity.InitializeID();

            entity.InsertTime = DateTime.Now;
            entity.UpdateTime = DateTime.Now;

            if (entity.Sequence == 0)
            {
                entity.Sequence = MaxSequence() + 1;
            }

            dbContext.Ef.Set<T>().Attach(entity);
            dbContext.Ef.Entry<T>(entity).State = EntityState.Added;

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 设置排序
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="order">方向</param>
        /// <returns>受影响行数</returns>
        public virtual int Order(string id, string order)
        {
            return Order(id, order, Query());
        }
    }
}