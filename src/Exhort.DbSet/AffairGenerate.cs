using Exhort.Configuration;
using Exhort.Dapper;
using Exhort.Entity;
using Exhort.Utility.Helper;
using System;
using System.Collections.Generic;

namespace Exhort.DbSet
{
    public partial class AffairContext : IDisposable
    {
        public Basic.BasicFunctionDbSet BasicFunction => DbSetByCache<Entity.Basic.BasicFunction, Basic.BasicFunctionDbSet>();
        public Basic.BasicMenuDbSet BasicMenu => DbSetByCache<Entity.Basic.BasicMenu, Basic.BasicMenuDbSet>();
        public Basic.BasicMenuFunctionDbSet BasicMenuFunction => DbSetByCache<Entity.Basic.BasicMenuFunction, Basic.BasicMenuFunctionDbSet>();
        public Basic.BasicRoleDbSet BasicRole => DbSetByCache<Entity.Basic.BasicRole, Basic.BasicRoleDbSet>();
        public Basic.BasicRoleMenuFunctionDbSet BasicRoleMenuFunction => DbSetByCache<Entity.Basic.BasicRoleMenuFunction, Basic.BasicRoleMenuFunctionDbSet>();
        public Basic.BasicSettingDbSet BasicSetting => DbSetByCache<Entity.Basic.BasicSetting, Basic.BasicSettingDbSet>();
        public Basic.BasicUserDbSet BasicUser => DbSetByCache<Entity.Basic.BasicUser, Basic.BasicUserDbSet>();
        public Basic.BasicUserRoleDbSet BasicUserRole => DbSetByCache<Entity.Basic.BasicUserRole, Basic.BasicUserRoleDbSet>();
    }
}