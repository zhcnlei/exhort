﻿using Exhort.Entity;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Exhort.DbSet
{
    /// <summary>
    /// 树形数据
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public class TreeDbSet<T> : OrderDbSet<T> where T : TreeEntity, new()
    {
        /// <summary>
        /// 更新对象的路径
        /// </summary>
        /// <param name="entity">实体</param>
        private void UpdatePath(T entity)
        {
            if (!entity.Pid.Equals("0"))
            {
                T parent = Get(entity.Pid);
                entity.Path = parent.Path + parent.ID + "|";
            }
            else { entity.Path = "|0|"; }
        }

        /// <summary>
        /// 添加实体重写
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public override int Added(T entity)
        {
            entity.InitializeID();

            entity.InsertTime = DateTime.Now;
            entity.UpdateTime = DateTime.Now;

            if (entity.Sequence == 0)
            {
                entity.Sequence = MaxSequence() + 1;
            }

            UpdatePath(entity);

            dbContext.Ef.Set<T>().Attach(entity);
            dbContext.Ef.Entry<T>(entity).State = EntityState.Added;

            return dbContext.NoTransactionCommit();
        }

        /// <summary>
        /// 更改实体重写
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public override int Modified(T entity)
        {
            entity.UpdateTime = DateTime.Now;

            List<T> entities = new List<T> { entity };

            T old = Get(entity.ID);

            if (entity.Pid != old.Pid)
            {
                UpdatePath(entity);

                var children = Children(old.ID).ToList();

                children.ForEach(m =>
                {
                    m.Path = m.Path.Replace(old.Path, entity.Path);
                });

                entities.AddRange(children);
            }

            return Modified(entities);
        }

        /// <summary>
        /// 主键删除实体重写
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>受影响行数</returns>
        public override int Deleted(string key)
        {
            return Deleted(EntityAndChildren(key));
        }

        /// <summary>
        /// 删除实体重写
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>受影响行数</returns>
        public override int Deleted(T entity)
        {
            return Deleted(EntityAndChildren(entity.ID));
        }

        /// <summary>
        /// 根据主键获取子集
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>列表</returns>
        public virtual List<T> Children(string key)
        {
            return Query().Where(m => m.Path.Contains("|" + key + "|")).ToList();
        }

        /// <summary>
        /// 根据主键获取对象和子集
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>列表</returns>
        public virtual List<T> EntityAndChildren(string key)
        {
            return Query().Where(m => m.ID == key || m.Path.Contains("|" + key + "|")).ToList();
        }

        /// <summary>
        /// 设置排序
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="order">方向</param>
        /// <param name="pid">父级</param>
        /// <returns>受影响行数</returns>
        public virtual int Order(string id, string order, string pid)
        {
            return Order(id, order, Query().Where(m => m.Pid == pid));
        }
    }
}