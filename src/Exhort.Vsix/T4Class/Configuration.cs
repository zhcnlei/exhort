﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Vsix.T4
{
    public partial class Configuration
    {
        private string _solution;
        private string _areaName;
        private string _modelName;

        public Configuration(string solution, string areaName, string modelName)
        {
            _solution = solution;
            _areaName = areaName;
            _modelName = modelName;
        }
    }
}