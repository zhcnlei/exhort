﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Vsix.T4
{
    public partial class ProxyRegistration
    {
        private string _solution;
        private List<EntityModel> _list;

        public ProxyRegistration(string solution, List<EntityModel> list)
        {
            _solution = solution;
            _list = list;
        }
    }
}
