﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Vsix.T4
{
    public partial class Service
    {
        private string _solution;
        private string _areaName;
        private string _modelName;
        private string _modelType;

        public Service(string solution, string areaName, string modelName, string modelType)
        {
            _solution = solution;
            _areaName = areaName;
            _modelName = modelName;
            _modelType = modelType;
        }
    }
}