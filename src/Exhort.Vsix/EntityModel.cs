﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Vsix
{
    public class EntityModel
    {
        public string AreaName { get; set; }
        public string TypeName { get; set; }
        public string EntityName { get; set; }

        public Type Type { get; set; }

        public EntityModel(string areaName, string typeName, string entityName, Type type)
        {
            this.AreaName = areaName;
            this.TypeName = typeName;
            this.EntityName = entityName;
            this.Type = type;
        }
    }
}