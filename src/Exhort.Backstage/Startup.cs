using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exhort.Utility.Helper;
using Exhort.Backstage.Components;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;


namespace Exhort.Backstage
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // 设置权限验证中间件
            services.AddAuthentication(ConfigHelper.CmsCookieScheme, "/work/home/authorize", false);

            // 设置 Session 的超时时间
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(120);
            });

            // 使用内存缓存
            services.AddMemoryCache();

            // 使用控制器和视图
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // 开发模式
            if (env.IsDevelopment() || ConfigHelper.IsDevelop)
            {
                // 异常信息
                app.UseDeveloperExceptionPage();
            }

            // 使用 Session 交互
            app.UseSession();

            // 权限验证
            app.UseAuthentication();

            // 使用 HTTPS 协议
            app.UseHttpsRedirection();

            // 使用静态文件
            app.UseStaticFiles();

            // 使用路由
            app.UseRouting();

            // 使用权限
            app.UseAuthorization();

            // 配置路由
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{area=work}/{controller=home}/{action=index}");
                endpoints.MapControllerRoute("areas", "{area:exists}/{controller=home}/{action=index}");
            });

            // 网站根目录设置
            ConfigHelper.RootPath = env.ContentRootPath;
        }
    }
}