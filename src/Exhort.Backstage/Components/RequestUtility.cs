﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Exhort.Backstage.Components
{
    public static class RequestUtility
    {
        public static bool QueryValue<T>(HttpContext context, string key, ref T value)
        {
            Microsoft.Extensions.Primitives.StringValues queryValues;

            if (context.Request.Query.TryGetValue(key, out queryValues))
            {
                try
                {
                    string str = queryValues.FirstOrDefault();

                    str = HttpUtility.UrlDecode(str);

                    value = (T)Convert.ChangeType(str, typeof(T)); return true;
                }
                catch { return false; }
            }

            return false;
        }
    }
}