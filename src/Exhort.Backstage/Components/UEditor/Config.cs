﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exhort.Backstage.Components.UEditor
{
    public static class Config
    {
        /* 提交的图片表单名称 */
        public static string ImageFieldName = "upfile";
        /* 上传大小限制，单位B */
        public static int ImageMaxSize = 2048000;
        /* 上传图片格式显示 */
        public static string[] ImageAllowFiles = { ".png", ".jpg", ".jpeg", ".gif", ".bmp" };
        /* 上传保存路径,可以自定义保存路径和文件名格式 */
        public static string ImagePathFormat = "/userfiles/ueditor/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}";
        /* {filename} 会替换成原文件名,配置这项需要注意中文乱码问题 */
        /* {rand:6} 会替换成随机数,后面的数字是随机数的位数 */
        /* {time} 会替换成时间戳 */
        /* {yyyy} 会替换成四位年份 */
        /* {yy} 会替换成两位年份 */
        /* {mm} 会替换成两位月份 */
        /* {dd} 会替换成两位日期 */
        /* {hh} 会替换成两位小时 */
        /* {ii} 会替换成两位分钟 */
        /* {ss} 会替换成两位秒 */
        /* 非法字符 \ : * ? " < > | */
        /* 具请体看线上文档: fex.baidu.com/ueditor/#use-format_upload_filename */

        /* 提交的图片表单名称 */
        public static string ScrawlFieldName = "upfile";
        /* 上传保存路径,可以自定义保存路径和文件名格式 */
        public static string ScrawlPathFormat = "/userfiles/ueditor/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}";
        /* 上传大小限制，单位B */
        public static int ScrawlMaxSize = 2048000;

        /* 上传保存路径,可以自定义保存路径和文件名格式 */
        public static string CatcherPathFormat = "/userfiles/ueditor/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}";

        /* 提交的视频表单名称 */
        public static string VideoFieldName = "upfile";
        /* 上传保存路径,可以自定义保存路径和文件名格式 */
        public static string VideoPathFormat = "/userfiles/ueditor/upload/video/{yyyy}{mm}{dd}/{time}{rand:6}";
        /* 上传大小限制，单位B，默认100MB */
        public static int VideoMaxSize = 102400000;
        /* 上传视频格式显示 */
        public static string[] VideoAllowFiles =
        {
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"
        };

        /* 提交的文件表单名称 */
        public static string FileFieldName = "upfile";
        /* 上传保存路径,可以自定义保存路径和文件名格式 */
        public static string FilePathFormat = "/userfiles/ueditor/upload/file/{yyyy}{mm}{dd}/{time}{rand:6}";
        /* 上传大小限制，单位B，默认50MB */
        public static int FileMaxSize = 51200000;
        /* 上传文件格式显示 */
        public static string[] FileAllowFiles =
        {
            ".png", ".jpg", ".jpeg", ".gif", ".bmp",
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
            ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
            ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
        };

        /* 指定要列出图片的目录 */
        public static string ImageManagerListPath = "/userfiles/ueditor/upload/image";
        /* 每次列出文件数量 */
        public static int ImageManagerListSize = 20;
        /* 列出的文件类型 */
        public static string[] ImageManagerAllowFiles = { ".png", ".jpg", ".jpeg", ".gif", ".bmp" };

        /* 指定要列出文件的目录 */
        public static string FileManagerListPath = "/userfiles/ueditor/upload/file";
        /* 列出的文件类型 */
        public static string[] FileManagerAllowFiles =
        {
            ".png", ".jpg", ".jpeg", ".gif", ".bmp",
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
            ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
            ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
        };
    }
}