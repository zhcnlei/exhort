﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exhort.Backstage.Components.UEditor
{
    public static class Entrance
    {
        public static void ProcessRequest(HttpContext context)
        {
            Handler action = null;

            switch (context.Request.Query["action"])
            {
                case "uploadimage":
                    action = new UploadHandler(context, new UploadConfig()
                    {
                        AllowExtensions = Config.ImageAllowFiles,
                        PathFormat = Config.ImagePathFormat,
                        SizeLimit = Config.ImageMaxSize,
                        UploadFieldName = Config.ImageFieldName
                    });
                    break;
                case "uploadscrawl":
                    action = new UploadHandler(context, new UploadConfig()
                    {
                        AllowExtensions = new string[] { ".png" },
                        PathFormat = Config.ScrawlPathFormat,
                        SizeLimit = Config.ScrawlMaxSize,
                        UploadFieldName = Config.ScrawlFieldName,
                        Base64 = true,
                        Base64Filename = "scrawl.png"
                    });
                    break;
                case "uploadvideo":
                    action = new UploadHandler(context, new UploadConfig()
                    {
                        AllowExtensions = Config.VideoAllowFiles,
                        PathFormat = Config.VideoPathFormat,
                        SizeLimit = Config.VideoMaxSize,
                        UploadFieldName = Config.VideoFieldName
                    });
                    break;
                case "uploadfile":
                    action = new UploadHandler(context, new UploadConfig()
                    {
                        AllowExtensions = Config.FileAllowFiles,
                        PathFormat = Config.FilePathFormat,
                        SizeLimit = Config.FileMaxSize,
                        UploadFieldName = Config.FileFieldName
                    });
                    break;
                case "listimage":
                    action = new ListFileManager(context, Config.ImageManagerListPath, Config.ImageManagerAllowFiles);
                    break;
                case "listfile":
                    action = new ListFileManager(context, Config.FileManagerListPath, Config.FileManagerAllowFiles);
                    break;
                case "catchimage":
                    action = new CrawlerHandler(context);
                    break;
                default:
                    action = new NotSupportedHandler(context);
                    break;
            }

            action.Process();
        }
    }
}