﻿using Exhort.Entity.Basic;
using Exhort.Resolver;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Backstage.Components
{
    public static class WorkControls
    {
        /// <summary>
        /// 控制器
        /// </summary>
        public static WorkController GetController(this RazorPage view)
        {
            return view.ViewBag.Work;
        }

        /// <summary>
        /// 后台菜单
        /// </summary>
        public static IHtmlContent MainMenu(string userid, bool isSuper)
        {
            var menus = ServiceResolver.BasicMenu.GetByUserId(userid, isSuper);

            return new HtmlString(MainChildrenMenus("0", menus));
        }

        /// <summary>
        /// 菜单拼装
        /// </summary>
        private static string MainChildrenMenus(string pid, List<BasicMenu> menus)
        {
            var list = menus.Where(m => m.Pid.Equals(pid) && !m.Type.Equals("embed"));

            StringBuilder html = new StringBuilder();

            foreach (var menu in list)
            {
                string attrs = $"v-on:click=\"menuClick\"";

                attrs += $" mid=\"{menu.ID}\"";
                attrs += $" title=\"{menu.Title}\"";
                attrs += $" type=\"{menu.Type}\"";
                attrs += $" module=\"{menu.Module + menu.Value}\"";
                attrs += $" src=\"{menu.Src}\"";

                string children = MainChildrenMenus(menu.ID, menus);

                if (!string.IsNullOrEmpty(children))
                {
                    html.Append($"<el-submenu index=\"{menu.ID}\" {attrs}>");
                    html.Append($"<template slot=\"title\">");
                    if (!string.IsNullOrEmpty(menu.Icon))
                    {
                        html.Append($"<i class=\"{menu.Icon}\"></i>");
                    }
                    html.Append($"<span slot=\"title\">{menu.Title}</span>");
                    html.Append($"</template>");
                    html.Append(children);
                    html.Append($"</el-submenu>");
                }
                else
                {
                    html.Append($"<el-menu-item index=\"{menu.ID}\" {attrs}>");
                    if (!string.IsNullOrEmpty(menu.Icon))
                    {
                        html.Append($"<i class=\"{menu.Icon}\"></i>");
                    }
                    html.Append($"<span slot=\"title\">{menu.Title}</span>");
                    html.Append($"</el-menu-item>");
                }
            }

            return html.ToString();
        }

        /// <summary>
        /// 页面工具按钮
        /// </summary>
        public static IHtmlContent PageButtons(this RazorPage view)
        {
            var work = view.GetController();

            StringBuilder html = new StringBuilder();

            string menuid = string.Empty;

            RequestUtility.QueryValue(work.HttpContext, "_mid", ref menuid);


            if (!string.IsNullOrEmpty(menuid))
            {
                var list = ServiceResolver.BasicFunction.GetByUserId(menuid, work.UserID, work.IsSuper);

                foreach (var item in list)
                {
                    html.Append($"<el-button icon=\"{item.Icon}\" size=\"small\" v-on:click=\"{item.Value}\" {item.Attrs}>{item.Title}</el-button>");
                }
            }

            return new HtmlString(html.ToString());
        }
    }
}