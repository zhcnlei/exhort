﻿using Exhort.Utility.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exhort.Backstage.Components
{
    [Authorize]
    public abstract class WorkController : Controller
    {
        public string CookieScheme
        {
            get { return ConfigHelper.CmsCookieScheme; }
        }

        public bool IsAuthenticated
        {
            get { return HttpContext.User.Identity.IsAuthenticated; }
        }

        public string UserID
        {
            get { return HttpContext.User.GetUserID(); }
        }

        public string UserName
        {
            get { return HttpContext.User.GetUserName(); }
        }

        public bool IsSuper
        {
            get { return HttpContext.User.GetIsSuper(); }
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            this.ViewBag.Work = this;

            base.OnActionExecuting(context);
        }
    }
}