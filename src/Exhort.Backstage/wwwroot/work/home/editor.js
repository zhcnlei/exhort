﻿Vue.component("page-editor", {
    data: function () {
        return { editor: null, isReady: false }
    },
    props: ['value', 'width', 'height', 'editorRef'],
    template: '<div class="page-editor" :ref="editorRef"></div>',
    methods: {
        init: function () {
            var that = this;
            var randomId = 'page-editor-' + Math.random().toString(16).substring(2);
            var el = eval("that.$refs." + that.editorRef);
            $("<div class='page-editor-el'>").attr("id", randomId).appendTo(el);
            that.editor = UE.getEditor(randomId, {
                snapscreenHost: "127.0.0.1",
                imageCompressEnable: false,
                compressSide: 1,
                catchRemoteImageEnable: !0,
                catchFieldName: "imgurl",
                toolbars: [
                    ["source", "|", "undo", "redo", "|", "fontfamily", "fontsize", "|", "blockquote", "horizontal", "removeformat", "formatmatch", "link", "unlink", "|", "insertvideo", "music", "insertimage", "drafts"],
                    ["bold", "italic", "underline", "forecolor", "backcolor", "|", "justifyleft", "justifycenter", "justifyright", "|", "rowspacingtop", "rowspacingbottom", "lineheight", "|", "insertorderedlist", "insertunorderedlist", "|", "imagenone", "imageleft", "imageright", "imagecenter"]
                ],
                labelMap: { anchor: "", undo: "" },
                //默认的编辑区域宽度
                initialFrameWidth: that.width || "100%",
                //默认的编辑区域高度
                initialFrameHeight: that.height || 400,
                //开启滚动条
                scaleEnabled: true,
                //关闭字数统计
                wordCount: false,
                //关闭elementPath
                elementPathEnabled: false
            });
            that.editor.addListener("ready", function () { that.ready(); });
            that.editor.addListener("contentChange", function () { that.change(); });
        },
        change: function () {
            this.$emit("input", this.editor.getContent());
        },
        ready: function () {
            this.editor.setContent(this.value);
        }

    },
    mounted: function () {
        this.init();
    },
    destroyed: function () {
        this.editor.destroy();
    }
});