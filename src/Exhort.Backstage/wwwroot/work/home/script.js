﻿var workVue = new Vue({
    el: "#work-app",
    data: {
        isCollapse: false,
        currentMenu: null,
        breadcrumb: [],
        page: { loading: false, template: null },
        dialog: {
            loading: false, title: "", width: '50%', visible: false, handle: function () { }, template: null
        }
    },
    components: {
        "page-error": {
            data: function () { return { request: { status: 0, responseText: "" } } },
            template: '<div class="p20"><div class="error-title">This request is {{request.status}}.</div><div class="error-content">{{request.responseText}}</div></div>'
        },
        "page-iframe": {
            data: function () { return { src: "" } },
            template: '<iframe class="page-iframe" :src="src"></iframe>'
        }
    },
    methods: {
        appInit: function () {
            var that = this;
            $(".work-init").fadeOut(500, function () {
                $(".work-init").remove();
            });
            $(".work-container").show();
            that.changePage("welcome", "/work/home/welcome");
        },
        menuInit: function () {
            var that = this;
            var elmenu = that.$refs.menu;
            if (elmenu.$children.length) {
                var menus = elmenu.$children;
                setTimeout(function () {
                    for (var menu of menus) {
                        if (menu.$children.length && !menu.active) {
                            elmenu.open(menu.index);
                        }
                    }
                }, 200);
            }
        },
        menuClick: function (menu) {
            var that = this;
            var attrs = menu.$attrs;
            switch (attrs.type) {
                case "href":
                    that.changeBreadcrumb(menu);
                    that.changePage(attrs.module, attrs.src, attrs.mid);
                    break;
                case "dialog":
                    that.changeDialog(attrs.title, attrs.module, attrs.src);
                    break;
                case "iframe":
                    that.changeBreadcrumb(menu);
                    that.changeIframe(attrs.src);
                    break;
                default: break;
            }
        },
        toWelcome: function () {
            var that = this;
            that.currentMenu = new Date().toString();
            that.breadcrumb = [];
            that.changePage("welcome", "/work/home/welcome");
        },
        userInfoCmd: function (command) {
            var that = this;
            switch (command) {
                case "myinfo":
                    that.changeDialog("个人信息", "basicuserUpdate", "/basic/user/update");
                    break;
                case "updatepwd":
                    that.changeDialog("修改密码", "basicuserUpdatepwd", "/basic/user/updatepwd");
                    break;
                case "logout":
                    that.$confirm("确认退出此账号登录？", "提示", {
                        type: "warning"
                    }).then(() => {
                        location.replace("/work/home/logout");
                    }).catch(() => { });
                    break;
                default: break;
            }
        },
        changeBreadcrumb: function (menu) {
            var setArr = function (arr, menu) {
                arr.push(menu.$attrs.title);
                if (!menu.isFirstLevel && menu.parentMenu) {
                    setArr(arr, menu.parentMenu);
                }
                return arr;
            }
            this.breadcrumb = setArr([], menu).reverse();
        },
        changePage: function (template, url, mid) {
            var that = this;
            $.ajax({
                url: url + "?" + $.param({ "_mid": mid }),
                type: "get", cache: false,
                beforeSend: function () {
                    that.page.loading = true;
                    that.page.template = null;
                },
                complete: function () {
                    that.page.loading = false;
                },
                success: function (result) {
                    $("#page-html-box").html(result);
                    that.page.template = eval(template);
                },
                error: function (request) {
                    that.ajaxComponentError("page", request);
                }
            });
        },
        changeDialog: function (title, template, url) {
            var that = this;
            that.dialog.title = title;
            that.dialog.visible = true;
            $.ajax({
                url: url, type: "get", cache: false,
                beforeSend: function () {
                    that.dialog.loading = true;
                },
                complete: function () {
                    that.dialog.loading = false;
                },
                success: function (result) {
                    var htmlBox = $("#dialog-html-box").html(result);
                    that.dialog.template = eval(template);
                    that.$nextTick(function () {
                        that.dialog.width = htmlBox.next().width() + 40 + "px";
                    });
                },
                error: function (request) {
                    that.ajaxComponentError("dialog", request);
                }
            });
        },
        changeIframe: function (url) {
            var that = this;
            that.page.template = "page-iframe";
            setTimeout(function () {
                that.$refs.pageComponent.src = url;
            }, 50);
        },
        changeCollapse: function () {
            var that = this;
            if (that.isCollapse) {
                that.menuInit();
            }
            that.isCollapse = !that.isCollapse;
        },
        dialogClose: function () {
            this.currentMenu = new Date().toString();
            this.dialog.handle = function () { };
            this.dialog.template = null;
        },
        ajax: function (options) {
            var that = this;
            var loading;
            $.ajax({
                type: options.type || "get",
                url: options.url,
                dataType: options.dataType || "json",
                data: options.data || {},
                cache: options.cache || true,
                success: function (result) {
                    if (!result.success) {
                        if (!options.failure) {
                            that.$message.error(result.message);
                        }
                        else { options.failure(result); }
                    }
                    else { options.success(result); }
                },
                beforeSend: function () {
                    if (options.target) {
                        loading = that.$loading({
                            target: options.target,
                            background: "rgba(255, 255, 255, 0.5)"
                        });
                    }
                },
                complete: function () {
                    if (options.target) { loading.close(); }
                },
                error: function (request) {
                    if (request.status == 400) {
                        that.ajaxLogout(request.responseText);
                    }
                    else { that.$message.error("系统错误"); }
                }
            });
        },
        ajaxLogout: function (text) {
            var that = this, json = JSON.parse(text);
            if (json.code && json.code == "Logout") {
                that.$message({
                    type: "error",
                    message: "登录超时，请重新登录",
                    onClose: function () {
                        location.replace("/work/home/authorize");
                    }
                });
            }
        },
        ajaxComponentError: function (component, request) {
            var that = this;
            if (request.status == 400) {
                that.ajaxLogout(request.responseText);
            }
            else {
                eval("that." + component).template = "page-error";
                that.$nextTick(function () {
                    eval("that.$refs." + component + "Component").request = request;
                });
            }
        }
    },
    mounted: function () {
        this.appInit();
        this.menuInit();
    }
});