﻿new Vue({
    el: "#login-box",
    data: {
        rules: {
            username: [
                { required: true, message: "请输入用户账号", trigger: "blur" },
                { min: 5, max: 15, message: "长度在 5 到 15 个字符", trigger: "blur" }
            ],
            password: [
                { required: true, message: "请输入登录密码", trigger: "blur" },
                { min: 6, max: 16, message: "长度在 6 到 16 个字符", trigger: "blur" }
            ]
        },
        isLogin: false,
        loading: false,
        loginForm: { username: "", password: "" }
    },
    methods: {
        login: function () {
            var that = this;
            that.$refs["loginForm"].validate((valid) => {
                if (valid && !that.isLogin) {
                    $.ajax({
                        url: "/work/home/login",
                        type: "post",
                        data: that.loginForm,
                        dataType: "json",
                        beforeSend: function () {
                            that.loading = true;
                        },
                        complete: function () {
                            that.loading = false;
                        },
                        success: function (result) {
                            if (result.success) {
                                that.$message({ message: "登录成功", type: "success" });
                                location.replace("/work/home/index");
                            }
                            else {
                                that.$message.error(result.message);
                            }
                            that.isLogin = result.success;
                        },
                        error: function () {
                            that.$message.error("系统错误，请稍后重试");
                        }
                    });
                }
                else { return false; }
            });
        }
    }
});