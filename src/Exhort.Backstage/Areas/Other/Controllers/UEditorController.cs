﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Exhort.Backstage.Areas.Other.Controllers
{
    [Area("Other")]
    public class UEditorController : Controller
    {
        public void Index()
        {
            Components.UEditor.Entrance.ProcessRequest(HttpContext);
        }
    }
}