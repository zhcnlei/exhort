﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exhort.Backstage.Components;
using Exhort.Entity.Basic;
using Exhort.Resolver;
using Exhort.Utility.Entities;
using Exhort.Utility.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Cray.Backstage.Areas.Basic.Controllers
{
    [Area("Basic")]
    public class RoleController : WorkController
    {
        public IActionResult Index()
        {
            return View();
        }

        public string Get(string id)
        {
            var entity = ServiceResolver.BasicRole.Get(id);

            return ServiceResult.Operation(entity != null, data: entity).Serialize();
        }

        public string Grid(int rows, int page)
        {
            var list = ServiceResolver.BasicRole.Grid(rows, page, out int total);

            return ServiceResult.Success(data: new { rows = list, total }).Serialize();
        }

        public string GridAll()
        {
            var list = ServiceResolver.BasicRole.Grid();

            return ServiceResult.Success(data: list).Serialize();
        }

        public string Save(BasicRole model)
        {
            return ServiceResolver.BasicRole.Save(model).Serialize();
        }

        public string Remove(string id)
        {
            return ServiceResolver.BasicRole.Delete(id).Serialize();
        }

        public string SetOrder(string id, string order)
        {
            return ServiceResolver.BasicRole.Order(id, order).Serialize();
        }

        public string Enable(string id, bool bit)
        {
            return ServiceResolver.BasicRole.Enable(id, bit).Serialize();
        }

        public string SetPower(string id, string[] list)
        {
            return ServiceResolver.BasicRoleMenuFunction.SetMenuFunction(id, list).Serialize();
        }

        public string PowerGrid(string id)
        {
            var menulist = ServiceResolver.BasicMenu.Grid();
            var functionlist = ServiceResolver.BasicFunction.Grid();
            var menufunctionlist = ServiceResolver.BasicMenuFunction.Grid();
            var rolemenufunctionlist = ServiceResolver.BasicRoleMenuFunction.GetByRoleId(id);

            List<Hashtable> AssembleList(string pid)
            {
                var menus = menulist.Where(m => m.Pid.Equals(pid));

                List<Hashtable> list = new List<Hashtable>();

                foreach (var menu in menus)
                {
                    Hashtable hash = new Hashtable();

                    hash.Add("id", menu.ID);
                    hash.Add("icon", menu.Icon);
                    hash.Add("title", menu.Title);
                    hash.Add("children", AssembleList(menu.ID));

                    var menufunctions = menufunctionlist.Where(m => m.MenuID == menu.ID);

                    List<Hashtable> powerlist = new List<Hashtable>();

                    foreach (var menufunction in menufunctions)
                    {
                        var function = functionlist.FirstOrDefault(m => m.ID == menufunction.FunctionID);

                        Hashtable powerhash = new Hashtable();

                        powerhash.Add("id", menufunction.ID);
                        powerhash.Add("icon", function.Icon);
                        powerhash.Add("title", function.Title);

                        powerhash.Add("exist", rolemenufunctionlist.Any(m => m.MenuFunctionID == menufunction.ID));

                        powerlist.Add(powerhash);
                    }

                    hash.Add("powerlist", powerlist);

                    list.Add(hash);
                }

                return list.Count > 0 ? list : null;
            }

            return ServiceResult.Success(data: AssembleList("0")).Serialize();
        }
    }
}