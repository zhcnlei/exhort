﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exhort.Backstage.Components;
using Exhort.Entity.Basic;
using Exhort.Resolver;
using Exhort.Utility.Entities;
using Exhort.Utility.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Exhort.Backstage.Areas.Basic.Controllers
{
    [Area("Basic")]
    public class MenuController : WorkController
    {
        public IActionResult Index()
        {
            return View();
        }

        public string Get(string id)
        {
            var entity = ServiceResolver.BasicMenu.Get(id);

            return ServiceResult.Operation(entity != null, data: entity).Serialize();
        }

        public string Grid()
        {
            var service = ServiceResolver.BasicMenu;

            var list = service.AssembleTree(service.Grid());

            return ServiceResult.Success(data: list).Serialize();
        }

        public string Save(BasicMenu model)
        {
            return ServiceResolver.BasicMenu.Save(model).Serialize();
        }

        public string Remove(string id)
        {
            return ServiceResolver.BasicMenu.Delete(id).Serialize();
        }

        public string SetOrder(string id, string order, string pid)
        {
            return ServiceResolver.BasicMenu.Order(id, order, pid).Serialize();
        }

        public string Enable(string id, bool bit)
        {
            return ServiceResolver.BasicMenu.Enable(id, bit).Serialize();
        }

        public string SetAssign(string id, string[] values)
        {
            return ServiceResolver.BasicMenuFunction.SetFunction(id, values).Serialize();
        }
    }
}