﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exhort.Backstage.Components;
using Exhort.Entity.Basic;
using Exhort.Resolver;
using Exhort.Utility.Entities;
using Exhort.Utility.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Exhort.Backstage.Areas.Basic.Controllers
{
    [Area("Basic")]
    public class FunctionController : WorkController
    {
        public IActionResult Index()
        {
            return View();
        }

        public string Get(string id)
        {
            var entity = ServiceResolver.BasicFunction.Get(id);

            return ServiceResult.Operation(entity != null, data: entity).Serialize();
        }

        public string Grid(int rows, int page)
        {
            var list = ServiceResolver.BasicFunction.Grid(rows, page, out int total);

            return ServiceResult.Success(data: new { rows = list, total }).Serialize();
        }

        public string GridAll()
        {
            var list = ServiceResolver.BasicFunction.Grid();

            return ServiceResult.Success(data: list).Serialize();
        }

        public string GridByMenu(string menuid)
        {
            var list = ServiceResolver.BasicFunction.GetByMenuId(menuid);

            return ServiceResult.Success(data: list.Select(m => m.ID)).Serialize();
        }

        public string Save(BasicFunction model)
        {
            return ServiceResolver.BasicFunction.Save(model).Serialize();
        }

        public string Remove(string id)
        {
            return ServiceResolver.BasicFunction.Delete(id).Serialize();
        }

        public string SetOrder(string id, string order)
        {
            return ServiceResolver.BasicFunction.Order(id, order).Serialize();
        }
    }
}