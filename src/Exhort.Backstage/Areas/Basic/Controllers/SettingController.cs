﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exhort.Backstage.Components;
using Exhort.Entity.Basic;
using Exhort.Resolver;
using Exhort.Utility.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Cray.Backstage.Areas.Basic.Controllers
{
    [Area("Basic")]
    public class SettingController : WorkController
    {
        private string[] tags = { "SecretKey", "DefaultRoleID", "Copyright" };

        public IActionResult Index()
        {
            return View();
        }

        public string Get()
        {
            var list = ServiceResolver.BasicSetting.GetByTags(tags);

            Hashtable hash = new Hashtable();

            foreach (var tag in tags)
            {
                var item = list.FirstOrDefault(m => m.Tag.Equals(tag));

                string content = string.Empty;

                if (item != null)
                {
                    content = item.Content;
                }

                hash.Add(tag, content);
            }

            return ServiceResult.Success(data: hash).Serialize();
        }

        public string Set(Dictionary<string, string> model)
        {
            var list = new List<BasicSetting>();

            foreach (var tag in tags)
            {
                list.Add(new BasicSetting
                {
                    Tag = tag,
                    Content = model[tag]
                });
            }

            return ServiceResolver.BasicSetting.SetContent(list).Serialize();
        }
    }
}