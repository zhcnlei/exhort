﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Exhort.Dapper
{
    public static class DbFactory
    {
        public static IDbConnection GetDbConnection(string connString, string providerName)
        {
            DbProviderFactory dbFactory;

            switch (providerName)
            {
                case "System.Data.SqlClient":
                    dbFactory = SqlClientFactory.Instance;
                    break;
                case "System.Data.MySqlClient":
                    dbFactory = MySqlClientFactory.Instance;
                    break;
                default: throw new Exception("unknown database provider.");
            }

            var dbConnection = dbFactory.CreateConnection();

            dbConnection.ConnectionString = connString;

            return dbConnection;
        }
    }
}