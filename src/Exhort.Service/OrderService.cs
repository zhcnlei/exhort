﻿using Exhort.DbSet;
using Exhort.Entity;
using Exhort.Utility.Entities;

namespace Exhort.Service
{
    /// <summary>
    /// 排序业务基类
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public abstract class OrderService<T> : BaseService<T> where T : OrderEntity, new()
    {
        /// <summary>
        /// 当前数据对象
        /// </summary>
        private OrderDbSet<T> dbSet
        {
            get { return db.DbSet<T, OrderDbSet<T>>(); }
        }

        /// <summary>
        /// 排序字段最大值
        /// </summary>
        /// <returns>最大值</returns>
        public virtual int MaxSequence()
        {
            return dbSet.MaxSequence();
        }

        /// <summary>
        /// 设置排序
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="order">顺序</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Order(string id, string order)
        {
            return LineResult(dbSet.Order(id, order), "排序成功调整", "没有可调换顺序的数据");
        }
    }
}
