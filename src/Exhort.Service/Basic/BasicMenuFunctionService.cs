using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicMenuFunctionService : OrderService<BasicMenuFunction>
    {
        public virtual ServiceResult SetFunction(string menuId, string[] functionIds)
        {
            db.BeginTransaction();

            db.BasicMenuFunction.Deleted(m => m.MenuID == menuId);

            for (int i = 0; i < functionIds.Length; i++)
            {
                string functionid = functionIds[i];

                if (!string.IsNullOrEmpty(functionid))
                {
                    var entity = new BasicMenuFunction();

                    entity.MenuID = menuId;
                    entity.FunctionID = functionid;
                    entity.Sequence = i + 1;

                    db.BasicMenuFunction.Added(entity);
                }
            }

            return CommitAndResult("���óɹ�");
        }
    }
}