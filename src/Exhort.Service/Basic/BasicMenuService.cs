using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicMenuService : TreeService<BasicMenu>
    {
        public virtual BasicMenu GetEmbed(string id, string tag)
        {
            return db.BasicMenu.Get(m => m.Type == "embed" && m.Pid == id && m.Value == tag);
        }

        public virtual List<BasicMenu> GetByUserId(string userId, bool isSuper)
        {
            return isSuper ? db.BasicMenu.EffectiveAll() : db.BasicMenu.EffectiveByUserId(userId);
        }

        public virtual ServiceResult Enable(string id, bool bit)
        {
            BasicMenu entity = db.BasicMenu.Get(id);

            if (entity != null)
            {
                if (entity.IsEnabled != bit)
                {
                    entity.IsEnabled = bit;

                    int line = db.BasicMenu.Modified(entity);

                    return LineResult(line, "启用成功", "禁用成功");
                }
                else
                {
                    return ServiceResult.Fail(bit ? "已经是启用状态" : "已经是禁用状态");
                }
            }

            return ServiceResult.Fail();
        }
    }
}