using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicRoleMenuFunctionService : BaseService<BasicRoleMenuFunction>
    {
        public virtual List<BasicRoleMenuFunction> GetByRoleId(string roleId)
        {
            return db.BasicRoleMenuFunction.Query(m => m.RoleID == roleId).ToList();
        }

        public virtual ServiceResult SetMenuFunction(string roleId, string[] menufunctionIds)
        {
            db.BeginTransaction();

            db.BasicRoleMenuFunction.Deleted(m => m.RoleID == roleId);

            foreach (var menufunctionid in menufunctionIds)
            {
                if (!string.IsNullOrEmpty(menufunctionid))
                {
                    var entity = new BasicRoleMenuFunction();

                    entity.RoleID = roleId;
                    entity.MenuFunctionID = menufunctionid;

                    db.BasicRoleMenuFunction.Added(entity);
                }
            }

            return CommitAndResult("���óɹ�");
        }
    }
}