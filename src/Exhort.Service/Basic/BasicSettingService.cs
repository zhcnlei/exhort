using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicSettingService : BaseService<BasicSetting>
    {
        public virtual BasicSetting GetByTag(string tag)
        {
            return db.BasicSetting.Get(m => m.Tag == tag);
        }

        public virtual string GetContent(string tag)
        {
            var entity = GetByTag(tag);

            if (entity != null)
            {
                return entity.Content;
            }

            return string.Empty;
        }

        public virtual List<BasicSetting> GetByTags(string[] tags)
        {
            return db.BasicSetting.Query(m => tags.Contains(m.Tag)).ToList();
        }

        public virtual ServiceResult SetContent(List<BasicSetting> list)
        {
            var items = GetByTags(list.Select(m => m.Tag).ToArray());

            db.BeginTransaction();

            foreach (var item in list)
            {
                var entity = items.FirstOrDefault(m => m.Tag == item.Tag);

                if (entity != null)
                {
                    entity.Content = item.Content;

                    db.BasicSetting.Modified(entity);
                }
                else { db.BasicSetting.Added(item); }
            }
            return CommitAndResult("���óɹ�");
        }
    }
}