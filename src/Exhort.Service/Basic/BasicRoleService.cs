using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicRoleService : OrderService<BasicRole>
    {
        public virtual List<BasicRole> Grid(int rows, int page, out int count)
        {
            return db.BasicRole.Pager(rows, page, out count).ToList();
        }

        public virtual ServiceResult Enable(string id, bool bit)
        {
            BasicRole entity = db.BasicRole.Get(id);

            if (entity != null)
            {
                if (entity.IsEnabled != bit)
                {
                    entity.IsEnabled = bit;

                    int line = db.BasicRole.Modified(entity);

                    return LineResult(line, "启用成功", "禁用成功");
                }
                else
                {
                    return ServiceResult.Fail(bit ? "已经是启用状态" : "已经是禁用状态");
                }
            }

            return ServiceResult.Fail();
        }
    }
}