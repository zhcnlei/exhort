using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicUserService : BaseService<BasicUser>
    {
        public virtual List<BasicUser> Grid(int rows, int page, string search, out int count)
        {
            var entities = db.BasicUser.Query(m => !m.IsSuper);

            if (!string.IsNullOrEmpty(search))
            {
                entities = entities.Where(m => m.UserName.Contains(search) || m.Name.Contains(search));
            }

            return db.BasicUser.Pager(rows, page, out count, entities).ToList();
        }

        public virtual ServiceResult SignIn(string username, string password)
        {
            password = EncryptHelper.Md5(password);

            var entity = db.BasicUser.Get(m => m.UserName == username && m.PassWord == password);

            if (entity != null)
            {
                if (entity.IsEnabled)
                {
                    return ServiceResult.Success(data: entity);
                }
                else { return ServiceResult.Fail("用户被禁用"); }
            }
            else { return ServiceResult.Fail("帐号或密码错误"); }
        }

        public override ServiceResult Save(BasicUser entity)
        {
            if (string.IsNullOrEmpty(entity.ID))
            {
                // 设置密码格式
                entity.PassWord = EncryptHelper.Md5(entity.PassWord);

                // 取得默认角色设置
                var roleid = db.BasicSetting.Get(m => m.Tag == "DefaultRoleID");

                db.BeginTransaction();

                db.BasicUser.Added(entity);

                if (roleid != null && !string.IsNullOrEmpty(roleid.Content))
                {
                    // 为用户设置默认角色
                    BasicUserRole bur = new BasicUserRole();
                    bur.RoleID = roleid.Content;
                    bur.UserID = entity.ID;
                    db.BasicUserRole.Added(bur);
                }

                return CommitAndResult("添加成功");
            }
            else
            {
                var old = db.BasicUser.Get(entity.ID);

                entity.UserName = old.UserName;
                entity.PassWord = old.PassWord;

                var line = db.BasicUser.Modified(entity);

                return LineResult(line, "修改成功");
            }
        }

        public virtual ServiceResult ResetPwd(string id, string pwd, string oper)
        {
            BasicUser entity = db.BasicUser.Get(id);

            if (entity != null)
            {
                entity.PassWord = EncryptHelper.Md5(pwd);
                var line = db.BasicUser.Modified(entity);
                ServiceResult result = LineResult(line, "重置成功");
                //日志记录
                result.Data = new
                {
                    Action = "ResetPwd",
                    Oper = oper,
                    entity.UserName
                };
                LogHelper.Info(result.Serialize());
                return result;
            }

            return ServiceResult.Fail();
        }

        public virtual ServiceResult UpdatePwd(string id, string oldpwd, string newpwd)
        {
            BasicUser entity = db.BasicUser.Get(id);

            if (entity != null)
            {
                if (entity.PassWord == EncryptHelper.Md5(oldpwd))
                {
                    entity.PassWord = EncryptHelper.Md5(newpwd);
                    var line = db.BasicUser.Modified(entity);
                    ServiceResult result = LineResult(line, "修改成功");
                    //日志记录
                    result.Data = new
                    {
                        Action = "UpdatePwd",
                        Oper = entity.UserName
                    };
                    LogHelper.Info(result.Serialize());
                    return result;
                }
                else { return ServiceResult.Fail("密码不正确"); }
            }

            return ServiceResult.Fail();
        }

        public virtual ServiceResult Enable(string id, bool bit)
        {
            BasicUser entity = db.BasicUser.Get(id);

            if (entity != null)
            {
                if (entity.IsEnabled != bit)
                {
                    entity.IsEnabled = bit;

                    var line = db.BasicUser.Modified(entity);

                    return LineResult(line, bit ? "启用成功" : "禁用成功");
                }
                else
                {
                    return ServiceResult.Fail(bit ? "已经是启用状态" : "已经是禁用状态");
                }
            }

            return ServiceResult.Fail();
        }
    }
}