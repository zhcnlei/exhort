using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicFunctionService : OrderService<BasicFunction>
    {
        public virtual List<BasicFunction> Grid(int rows, int page, out int count)
        {
            return db.BasicFunction.Pager(rows, page, out count).ToList();
        }

        public virtual List<BasicFunction> GetByMenuId(string menuId)
        {
            return db.BasicFunction.GetByMenuId(menuId);
        }

        public virtual List<BasicFunction> GetByUserId(string menuId, string userId, bool isSuper)
        {
            return isSuper ? db.BasicFunction.EffectiveAll(menuId) : db.BasicFunction.EffectiveByUserId(menuId, userId);
        }
    }
}