using Exhort.Entity.Basic;
using Exhort.Utility.Helper;
using Exhort.Utility.Entities;
using Exhort.Utility.Extensions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Exhort.Service.Basic
{
    public class BasicUserRoleService : BaseService<BasicUserRole>
    {
        public virtual List<BasicUserRole> Grid(string userId)
        {
            return db.BasicUserRole.Query(m => m.UserID == userId).Include(m => m.BasicRole).ToList();
        }

        public virtual ServiceResult SetRoles(string userId, string[] roleIds)
        {
            db.BeginTransaction();

            db.BasicUserRole.Deleted(m => m.UserID == userId);

            for (int i = 0; i < roleIds.Length; i++)
            {
                string roleid = roleIds[i];

                if (!string.IsNullOrEmpty(roleid))
                {
                    var entity = new BasicUserRole();

                    entity.UserID = userId;
                    entity.RoleID = roleid;

                    db.BasicUserRole.Added(entity);
                }
            }

            return CommitAndResult("���óɹ�");
        }
    }
}