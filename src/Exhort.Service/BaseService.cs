﻿using Exhort.Entity;
using Exhort.Utility.Helper;
using Exhort.Aop;
using Exhort.Utility.Entities;
using Exhort.DbSet;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exhort.Service
{
    /// <summary>
    /// 基本业务基类
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    public abstract class BaseService<T> : ProxyTarget where T : BaseEntity, new()
    {
        /// <summary>
        /// 当前数据对象
        /// </summary>
        private BaseDbSet<T> dbSet
        {
            get { return db.DbSet<T, BaseDbSet<T>>(); }
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        /// <returns></returns>
        internal bool Commit()
        {
            try
            {
                db.Commit();

                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(this, ex);
            }

            return false;
        }

        /// <summary>
        /// 提交并回馈
        /// </summary>
        /// <param name="message"></param>
        /// <param name="failure"></param>
        /// <returns></returns>
        internal ServiceResult CommitAndResult(string message = "操作成功", string failure = "操作失败", object data = null)
        {
            if (Commit())
            {
                return new ServiceResult(ServiceResultCode.Success, message, data);
            }
            {
                return new ServiceResult(ServiceResultCode.Fail, failure, data);
            }
        }

        /// <summary>
        /// 受影响行反馈
        /// </summary>
        /// <param name="line"></param>
        /// <param name="message"></param>
        /// <param name="failure"></param>
        /// <returns></returns>
        internal ServiceResult LineResult(int line, string message = "操作成功", string failure = "操作失败")
        {
            if (line > 0)
            {
                return new ServiceResult(ServiceResultCode.Success, message);
            }
            {
                return new ServiceResult(ServiceResultCode.Fail, failure);
            }
        }

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="key">主键</param>
        /// <returns>实体</returns>
        public virtual T Get(string key)
        {
            return dbSet.Get(key);
        }

        /// <summary>
        /// 实体列表
        /// </summary>
        /// <returns>实体列表</returns>
        public virtual List<T> Grid()
        {
            return dbSet.Query().ToList();
        }

        /// <summary>
        /// 保存实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Save(T entity)
        {
            if (string.IsNullOrEmpty(entity.ID))
            {
                return Insert(entity);
            }
            else { return Update(entity); }
        }

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Insert(T entity)
        {
            return LineResult(dbSet.Added(entity), "添加成功");
        }

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Insert(IEnumerable<T> entities)
        {
            return LineResult(dbSet.Added(entities), "添加成功");
        }

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Update(T entity)
        {
            return LineResult(dbSet.Modified(entity), "修改成功");
        }

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Update(IEnumerable<T> entities)
        {
            return LineResult(dbSet.Modified(entities), "修改成功");
        }

        /// <summary>
        /// 主键删除实体
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Delete(string key)
        {
            return LineResult(dbSet.Deleted(key), "删除成功");
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Delete(T entity)
        {
            return LineResult(dbSet.Deleted(entity), "删除成功");
        }

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体</param>
        /// <returns>操作结果</returns>
        public virtual ServiceResult Delete(IEnumerable<T> entities)
        {
            return LineResult(dbSet.Deleted(entities), "删除成功");
        }
    }
}