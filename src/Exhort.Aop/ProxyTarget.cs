﻿using Exhort.DbSet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Aop
{
    public abstract class ProxyTarget
    {
        private AffairContext dbContext;

        /// <summary>
        /// 数据上下文
        /// </summary>
        public AffairContext db
        {
            get
            {
                if (dbContext == null)
                {
                    dbContext = new AffairContext();
                }
                return dbContext;
            }
        }

        /// <summary>
        /// 前置
        /// </summary>
        public void Before()
        {

        }

        /// <summary>
        /// 后置
        /// </summary>
        public void After()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }
    }
}