﻿using Castle.DynamicProxy;
using Exhort.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Aop
{
    /// <summary>
    /// 拦截器
    /// </summary>
    public class Interceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var target = invocation.InvocationTarget as ProxyTarget;

            try
            {
                target.Before();

                invocation.Proceed();
            }
            catch (Exception ex)
            {
                LogHelper.Error(target, ex);
            }
            finally
            {
                target.After();
            }

            // 拦截使用 Castle DynamicProxy 实现
            // 避免被代理函数需要使用 virtual , 给 Service 增加了接口通过 IoC 进行注册
        }
    }
}