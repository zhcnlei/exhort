﻿using Exhort.Utility.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exhort.Utility.Entities
{
    /// <summary>
    /// 服务结果
    /// </summary>
    public class ServiceResult
    {
        /// <summary>
        /// 结果代码
        /// </summary>
        public ServiceResultCode Code { get; set; }

        /// <summary>
        /// 文本消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据对象
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public ServiceResult() { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="code">结果代码</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public ServiceResult(ServiceResultCode code, string message = null, object data = null)
        {
            this.Code = code; this.Message = message; this.Data = data;
        }

        /// <summary>
        /// 成功实例
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ServiceResult Success(string message = null, object data = null)
        {
            return new ServiceResult(ServiceResultCode.Success, message, data);
        }

        /// <summary>
        /// 失败实例
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ServiceResult Fail(string message = null, object data = null)
        {
            return new ServiceResult(ServiceResultCode.Fail, message, data);
        }

        /// <summary>
        /// 操作实例
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ServiceResult Operation(bool success, string message = null, object data = null)
        {
            var code = success ? ServiceResultCode.Success : ServiceResultCode.Fail;

            return new ServiceResult(code, message, data);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns>序列化对象字符串</returns>
        public string Serialize(string[] retain = null, string[] ignore = null, IDictionary<string, string> turns = null)
        {
            return JsonHelper.Serialize(new { code = Code.ToString(), success = Code.Equals(ServiceResultCode.Success), message = Message, data = Data }, retain, ignore, turns);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="code">结果代码</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        /// <returns>序列化对象字符串</returns>
        public static string Serialize(ServiceResultCode code, string message = null, object data = null)
        {
            return new ServiceResult(code, message, data).Serialize();
        }
    }

    /// <summary>
    /// 结果代码
    /// 可能会陆续补充更多类型
    /// </summary>
    public enum ServiceResultCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success,
        /// <summary>
        /// 失败
        /// </summary>
        Fail,
        /// <summary>
        /// 错误
        /// </summary>
        Error,
        /// <summary>
        /// 登录超时
        /// </summary>
        Logout,
        /// <summary>
        /// 超时
        /// </summary>`
        Timeout,
        /// <summary>
        /// 未知
        /// </summary>
        Unknown,
        /// <summary>
        /// 未认证
        /// </summary>
        UnAuthorized = -1
    }
}
