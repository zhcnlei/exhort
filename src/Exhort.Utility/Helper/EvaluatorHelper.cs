﻿using System;
using System.CodeDom.Compiler;
using System.Reflection;

namespace Exhort.Utility.Helper
{
    /// <summary>
    /// 类似js的eval方法
    /// </summary>
    public static class EvaluatorHelper
    {
        /// <summary>
        /// JScript代码
        /// </summary>
        private static readonly string _source = @"class Evaluator { public function Eval(expr : String) : String { return eval(expr); } }";

        /// <summary>
        /// 计算结果,如果表达式出错则抛出异常
        /// </summary>
        /// <param name="statement">表达式,如"1+2+3+4"</param>
        /// <returns>结果</returns>
        public static object Eval(string statement)
        {
            //构造JScript的编译驱动代码
            CodeDomProvider provider = CodeDomProvider.CreateProvider("JScript");

            CompilerParameters parameters;
            parameters = new CompilerParameters();
            parameters.GenerateInMemory = true;

            CompilerResults results;
            results = provider.CompileAssemblyFromSource(parameters, _source);

            Assembly assembly = results.CompiledAssembly;
            Type _evaluatorType = assembly.GetType("Evaluator");

            object _evaluator = Activator.CreateInstance(_evaluatorType);

            return _evaluatorType.InvokeMember("Eval", BindingFlags.InvokeMethod, null, _evaluator, new object[] { statement });
        }
    }
}
