﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Exhort.Utility.Helper
{
    /// <summary>
    /// 图片操作
    /// </summary>
    public static class ThumbnailHelper
    {
        /// <summary>
        /// 生成缩略图
        /// </summary>
        /// <param name="orginalImagePat">原图片地址</param>
        /// <param name="thumNailPath">缩略图地址</param>
        /// <param name="width">缩略图宽度</param>
        /// <param name="height">缩略图高度</param>
        /// <param name="model">生成缩略的模式</param>
        public static void Generate(string originalImagePath, string thumNailPath, int width, int height, TnType model, int quality = 97)
        {
            Image originalImage = Image.FromFile(originalImagePath);

            int thumWidth = width;      //缩略图的宽度
            int thumHeight = height;    //缩略图的高度

            int x = 0;
            int y = 0;

            int originalWidth = originalImage.Width;    //原始图片的宽度
            int originalHeight = originalImage.Height;  //原始图片的高度

            switch (model)
            {
                case TnType.HW:
                    break;
                case TnType.W:
                    thumHeight = originalImage.Height * width / originalImage.Width;
                    break;
                case TnType.H:
                    thumWidth = originalImage.Width * height / originalImage.Height;
                    break;
                case TnType.Cut:
                    if ((double)originalImage.Width / (double)originalImage.Height > (double)thumWidth / (double)thumHeight)
                    {
                        originalHeight = originalImage.Height;
                        originalWidth = originalImage.Height * thumWidth / thumHeight;
                        y = 0;
                        x = (originalImage.Width - originalWidth) / 2;
                    }
                    else
                    {
                        originalWidth = originalImage.Width;
                        originalHeight = originalWidth * height / thumWidth;
                        x = 0;
                        y = (originalImage.Height - originalHeight) / 2;
                    }
                    break;
                case TnType.MW:
                    if (thumWidth > originalWidth)
                    {
                        thumWidth = originalImage.Width;
                        thumHeight = originalImage.Height;
                    }
                    else
                    {
                        thumHeight = originalImage.Height * width / originalImage.Width;
                    }
                    break;
                case TnType.MH:
                    if (thumHeight > originalHeight)
                    {
                        thumWidth = originalImage.Width;
                        thumHeight = originalImage.Height;
                    }
                    else
                    {
                        thumWidth = originalImage.Width * height / originalImage.Height;
                    }
                    break;
                default:
                    break;
            }

            //新建一个bmp图片
            Image bitmap = new Bitmap(thumWidth, thumHeight);

            //新建一个画板
            Graphics graphic = Graphics.FromImage(bitmap);

            //设置高质量查值法
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //设置高质量，低速度呈现平滑程度
            graphic.SmoothingMode = SmoothingMode.HighQuality;

            //清空画布并以透明背景色填充
            graphic.Clear(Color.White);

            //在指定位置并且按指定大小绘制原图片的指定部分
            graphic.DrawImage(originalImage,
                new Rectangle(0, 0, thumWidth, thumHeight),
                new Rectangle(x, y, originalWidth, originalHeight),
                GraphicsUnit.Pixel);

            //定义jpegICI部分
            EncoderParameters encoderParams = new EncoderParameters();
            EncoderParameter encoderParam = new EncoderParameter(Encoder.Quality, new long[] { quality });

            encoderParams.Param[0] = encoderParam;
            //获得包含有关内置图像编码解码器的信息的 ImageCodecInfo 对象。
            ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();

            ImageCodecInfo jpegICI = null;

            foreach (var item in arrayICI)
            {
                if (item.FormatDescription.ToLower().Equals("jpeg"))
                {
                    jpegICI = item; break;
                }
            }

            try
            {
                bitmap.Save(thumNailPath, jpegICI, encoderParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                originalImage.Dispose(); bitmap.Dispose(); graphic.Dispose();
            }
        }

        /// <summary>
        /// 在图片上添加文字水印
        /// </summary>
        /// <param name="syWord">要添加的水印文字</param>
        /// <param name="path">要添加水印的图片路径</param>
        /// <param name="syPath">生成的水印图片存放的位置</param>
        public static void WaterWord(string syWord, string path, string syPath)
        {
            Image image = Image.FromFile(path);

            //新建一个画板
            Graphics graphic = Graphics.FromImage(image);
            graphic.DrawImage(image, 0, 0, image.Width, image.Height);

            //设置字体
            Font f = new Font("Verdana", 12);

            //设置字体颜色
            Brush b = new SolidBrush(Color.Red);

            graphic.DrawString(syWord, f, b, 110, 150);
            graphic.Dispose();

            //保存文字水印图片
            image.Save(syPath); image.Dispose();
        }

        /// <summary>
        /// 在图片上添加图片水印
        /// </summary>
        /// <param name="path">原服务器上的图片路径</param>
        /// <param name="syPicPath">水印图片的路径</param>
        /// <param name="waterPicPath">生成的水印图片存放路径</param>
        public static void WaterPic(string path, string syPicPath, string waterPicPath)
        {
            Image image = Image.FromFile(path);
            Image waterImage = Image.FromFile(syPicPath);
            Graphics graphic = Graphics.FromImage(image);

            graphic.DrawImage(waterImage, new Rectangle((image.Width - waterImage.Width) / 2,
                (image.Height - waterImage.Height) / 2,
                waterImage.Width, waterImage.Height),
                0, 0, waterImage.Width, waterImage.Height, GraphicsUnit.Pixel);

            graphic.Dispose();

            image.Save(waterPicPath); image.Dispose();
        }
    }

    /// <summary>
    /// 缩略图生成方式
    /// </summary>
    public enum TnType
    {
        /// <summary>
        /// 指定高宽缩放,可能变形
        /// </summary>
        HW,
        /// <summary>
        /// 指定宽度,高度按照比例缩放
        /// </summary>
        W,
        /// <summary>
        /// 指定高度,宽度按照等比例缩放
        /// </summary>
        H,
        /// <summary>
        /// 指定高宽裁剪,剧中裁剪
        /// </summary>
        Cut,
        /// <summary>
        /// 指定最大宽度,超出后按最大宽度缩放
        /// </summary>
        MW,
        /// <summary>
        /// 指定最大高度,超出后按最大高度缩放
        /// </summary>
        MH
    }
}