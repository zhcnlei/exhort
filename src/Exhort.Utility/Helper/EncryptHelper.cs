﻿using NETCore.Encrypt;

namespace Exhort.Utility.Helper
{
    public static class EncryptHelper
    {
        public static string Md5(string srcString)
        {
            return EncryptProvider.Md5(srcString, MD5Length.L32);
        }
        public static string Sha1(string str)
        {
            return EncryptProvider.Sha1(str);
        }
    }
}