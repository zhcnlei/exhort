﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Exhort.Utility.Helper
{
    /// <summary>
    /// 配置文件
    /// </summary>
    public static class ConfigHelper
    {
        private static IConfiguration configuration { get; set; }
        private static string rootPath { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        static ConfigHelper()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            configuration = builder.Build();
        }

        /// <summary>
        /// 站点根目录
        /// </summary>
        public static string RootPath
        {
            get
            {
                if (string.IsNullOrEmpty(rootPath))
                {
                    rootPath = Directory.GetCurrentDirectory();
                }

                return rootPath;
            }
            set { rootPath = value; }
        }

        /// <summary>
        /// 读取配置文件
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetByKey(string key)
        {
            return configuration[key];
        }

        /// <summary>
        /// 是否是开发模式
        /// </summary>
        public static bool IsDevelop
        {
            get
            {
                return Convert.ToBoolean(configuration["isDevelop"]);
            }
        }

        /// <summary>
        /// 认证授权 CookieScheme
        /// </summary>
        public static string CmsCookieScheme
        {
            get
            {
                return Convert.ToString(configuration["cmsCookieScheme"]);
            }
        }
    }
}