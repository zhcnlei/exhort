﻿using log4net;
using log4net.Config;
using System;
using System.IO;

namespace Exhort.Utility.Helper
{
    /// <summary>
    /// 日志记录工具 
    /// </summary>
    public static class LogHelper
    {
        private const string repositoryName = "NETCoreRepository";

        /// <summary>
        /// 构造函数
        /// </summary>
        static LogHelper()
        {
            var repository = LogManager.CreateRepository(repositoryName);

            var fileinfo = new FileInfo($"{Directory.GetCurrentDirectory()}/log4net.config");

            XmlConfigurator.ConfigureAndWatch(repository, fileinfo);
        }

        /// <summary>
        /// 日志对象
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static ILog GetLogger(string name)
        {
            return LogManager.GetLogger(repositoryName, name);
        }

        /// <summary>
        /// 日志对象
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static ILog GetLogger(Type type)
        {
            return LogManager.GetLogger(repositoryName, type);
        }

        /// <summary>
        /// 记录Info日志
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Info(string msg)
        {
            GetLogger("系统消息").Info(msg);
        }

        /// <summary>
        /// 记录Info日志
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="msg">消息</param>
        public static void Info(object obj, string msg)
        {
            GetLogger(obj.GetType()).Info(msg);
        }

        /// <summary>
        /// 记录Debug日志
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Debug(string msg)
        {
            GetLogger("系统消息").Debug(msg);
        }

        /// <summary>
        /// 记录Debug日志
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="msg">消息</param>
        public static void Debug(object obj, string msg)
        {
            GetLogger(obj.GetType()).Debug(msg);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="ex">异常</param>
        public static void Error(Exception ex)
        {
            GetLogger(ex.Source).Error(ex.Message, ex);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="obj">某个类对象</param>
        /// <param name="ex">异常</param>
        public static void Error(object obj, Exception ex)
        {
            GetLogger(obj.GetType()).Error(ex.Message, ex);
        }
    }
}