using Castle.Windsor;

namespace Exhort.Resolver
{
    public static partial class ServiceResolver
    {
        public static Service.Basic.BasicFunctionService BasicFunction => DependencyResolver.For<Service.Basic.BasicFunctionService>();
        public static Service.Basic.BasicMenuService BasicMenu => DependencyResolver.For<Service.Basic.BasicMenuService>();
        public static Service.Basic.BasicMenuFunctionService BasicMenuFunction => DependencyResolver.For<Service.Basic.BasicMenuFunctionService>();
        public static Service.Basic.BasicRoleService BasicRole => DependencyResolver.For<Service.Basic.BasicRoleService>();
        public static Service.Basic.BasicRoleMenuFunctionService BasicRoleMenuFunction => DependencyResolver.For<Service.Basic.BasicRoleMenuFunctionService>();
        public static Service.Basic.BasicSettingService BasicSetting => DependencyResolver.For<Service.Basic.BasicSettingService>();
        public static Service.Basic.BasicUserService BasicUser => DependencyResolver.For<Service.Basic.BasicUserService>();
        public static Service.Basic.BasicUserRoleService BasicUserRole => DependencyResolver.For<Service.Basic.BasicUserRoleService>();
    }
}