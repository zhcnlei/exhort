using Castle.MicroKernel;
using Castle.MicroKernel.Registration;

namespace Exhort.Resolver
{
    public partial class ProxyRegistration : IRegistration
    {
        public ProxyRegistration()
        {
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicFunctionService, Service.Basic.BasicFunctionService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicMenuService, Service.Basic.BasicMenuService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicMenuFunctionService, Service.Basic.BasicMenuFunctionService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicRoleService, Service.Basic.BasicRoleService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicRoleMenuFunctionService, Service.Basic.BasicRoleMenuFunctionService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicSettingService, Service.Basic.BasicSettingService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicUserService, Service.Basic.BasicUserService>());
            registerGenerateList.Add(GetTargetRegistration<Service.Basic.BasicUserRoleService, Service.Basic.BasicUserRoleService>());
        }
    }
}