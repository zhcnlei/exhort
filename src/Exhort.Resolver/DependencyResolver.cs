﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exhort.Resolver
{
    /// <summary>
    /// 依赖解析器
    /// </summary>
    public static partial class DependencyResolver
    {
        private static IWindsorContainer container;

        /// <summary>
        /// 初始化
        /// </summary>
        static DependencyResolver()
        {
            container = new WindsorContainer();
            container.Register(new ProxyRegistration());
        }

        /// <summary>
        /// 提取服务
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T For<T>()
        {
            return container.Resolve<T>();
        }
    }
}